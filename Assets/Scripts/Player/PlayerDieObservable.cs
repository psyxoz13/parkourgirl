using System.Collections.Generic;

public class PlayerDieObservable : IPlayerDieObservable
{
    private static List<IPlayerDieObserver> _observers = new List<IPlayerDieObserver>();

    public void AddObserver(IPlayerDieObserver observer)
    {
        _observers.Add(observer);
    }

    public void RemoveObserver(IPlayerDieObserver observer)
    {
        _observers.Remove(observer);
    }

    public void OnPlayerDie(Player player)
    {
        for (int i = 0; i < _observers.Count; i++)
        {
            _observers[i].OnPlayerDie(player);
        }
    }
}
