using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static PlayerDieObservable PlayerDieObservable = new PlayerDieObservable();

    [SerializeField] private MobileInput _mobileInput;
    [SerializeField] private PlayerAnimation _animation;
    [SerializeField] private Movement _movement;

    private IAction _defaultAction = new DefaultAction();
    private IAction _action;

    private void Start()
    {
        _action = _defaultAction;

        _mobileInput.OnTap.AddListener(OnTap);
    }

    public void SetCurrentAction(IAction action)
    {
        _action = action;
    }

    public void SetDefaultAction()
    {
        _defaultAction.SetPlayerAction(this);
    }

    public void StartRun()
    {
        _animation.SetTrigger("SetRun");

        PlayerMachineState.OnStateChanged.AddListener(TryActivateGameComponents);
    }

    private void OnTap()
    {
        if (PlayerMachineState.CurrentState == MachineState.Run && _movement.TryJump(_action.MoveStruct))
        {
            _animation.SetTrigger(_action.AnimationTrigger);
            _action = _defaultAction;
        }
    }

    private void TryActivateGameComponents(MachineState machineState)
    {
        if (machineState != MachineState.Scene)
        {
            _mobileInput.enabled = true;
            _animation.enabled = true;
            _movement.enabled = true;

            StartCoroutine(SetAnimationValues());
            StartCoroutine(TryDie());

            PlayerMachineState.OnStateChanged.RemoveListener(TryActivateGameComponents);
        }
    }

    private IEnumerator TryDie()
    {
        yield return new WaitForSeconds(1f);

        while (true)
        {
            if (_movement.ForwardOffset <= .01f || _movement.Velocity.y <= -15f)
            {
                DisableMoveComponents();
                PlayerDieObservable.OnPlayerDie(this);

                break;
            }

            yield return null;
        }
    }

    private IEnumerator SetAnimationValues()
    {
        while (true)
        {
            _animation.SetStrafe(_mobileInput.Acceleration.x);
            _animation.SetFallVelocity(_movement.Velocity.y);
            _animation.SetGrounded(_movement.IsGrounded);

            yield return null;
        }
    }

    private void DisableMoveComponents()
    {
        _animation.enabled = false;
        _movement.enabled = false;
    }
}
