using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class PlayerTrigger : MonoBehaviour
{
    public UnityEvent<Player> OnEnter = new UnityEvent<Player>();
    public UnityEvent<Player> OnExit = new UnityEvent<Player>();

    [SerializeField] private bool _once = true;

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            OnEnter?.Invoke(player);

            if (_once)
            {
                gameObject.SetActive(false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            OnExit?.Invoke(player);
        }
    }
}
