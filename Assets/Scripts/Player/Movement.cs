using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(CharacterController), typeof(MobileInput))]
public class Movement : MonoBehaviour
{
    public Vector3 Velocity => _characterController.velocity;
    public float ForwardOffset { get; private set; }
    public bool IsGrounded => _characterController.isGrounded;

    [SerializeField] private float _frontMovementSpeed = 5f;
    [SerializeField] private float _speedAcceleration = 5f;

    [Header("Strafe")]
    [SerializeField] private float _runStrafeSpeed = 15f;
    [SerializeField] private float _actionStrafeSpeed = 5f;
    [SerializeField] private float _strafeAccelerateDuration = .5f;

    [Space]
    [SerializeField] private float _strafeLimit = 2.7f;

    private CharacterController _characterController;
    private MobileInput _mobileInput;

    private Vector3 _velocity;
    private Vector3 _movementDirection;

    private float _forceMultiplier = 1;
    private float _strafeSpeed;

    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _mobileInput = GetComponent<MobileInput>();
    }

    private void OnEnable()
    {
        _characterController.enabled = true;
        PlayerMachineState.OnStateChanged.AddListener(OnStateChanged);
    }

    public void OnDisable()
    {
        _characterController.enabled = false;
        PlayerMachineState.OnStateChanged.RemoveListener(OnStateChanged);
    }

    private void Start()
    {
        _strafeSpeed = _runStrafeSpeed;
    }

    private void Update()
    {
        var oldPositionZ = transform.position.z;

        Move();
        SetLimitedPosition();

        ForwardOffset = transform.position.z - oldPositionZ;
    }

    public bool TryJump(MoveStruct move)
    {
        if (_characterController.isGrounded)
        {
            _velocity.y = Mathf.Sqrt(move.Height * -3.0f * Physics.gravity.y);       
            _velocity += move.AdditionalVelocity;

            _forceMultiplier = move.ForceMultiplier;

            return true;
        }
        return false;
    }

    public void SetForwardForceMultiplier(float multiplier)
    {
        _forceMultiplier = multiplier;
    }

    private void OnStateChanged(MachineState machineState)
    {
        if (machineState == MachineState.Run)
        {
            DOTween.To(() => _strafeSpeed, x => _strafeSpeed = x, _runStrafeSpeed, _strafeAccelerateDuration);
            _forceMultiplier = 1f;
        }
        else if (machineState == MachineState.Action)
        {
            DOTween.To(() => _strafeSpeed, x => _strafeSpeed = x, _actionStrafeSpeed, _strafeAccelerateDuration);
        }
    }

    private void Move()
    {
        _movementDirection.x = _mobileInput.Acceleration.x * _strafeSpeed;
        _movementDirection.z = Mathf.Lerp(
            _movementDirection.z,
            _frontMovementSpeed * _forceMultiplier,
            _speedAcceleration * Time.deltaTime);

        _movementDirection.y = 0f;

        _characterController.Move(_movementDirection * Time.deltaTime);

        _velocity.y += Physics.gravity.y * Time.deltaTime;
        _characterController.Move(_velocity * Time.deltaTime);

        if (_characterController.isGrounded && _velocity.y < -1f)
        {
            _velocity = Vector3.zero;
            _velocity.y = -1f;
        }
    }

    private void SetLimitedPosition()
    {
        transform.position = new Vector3(
            Mathf.Clamp(
                transform.position.x,
                -_strafeLimit,
                _strafeLimit),
            transform.position.y,
            transform.position.z);
    }
}

[System.Serializable]
public struct MoveStruct
{
    public float Height;
    public float ForceMultiplier;

    public Vector3 AdditionalVelocity;
}
