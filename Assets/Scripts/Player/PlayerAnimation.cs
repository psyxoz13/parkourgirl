using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerAnimation : MonoBehaviour
{
    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        _animator.enabled = true;
    }

    private void OnDisable()
    {
        _animator.enabled = false;
    }

    public void SetTrigger(string trigger)
    {
        _animator.SetTrigger(trigger);
    }

    public void SetStrafe(float strafe)
    {
        _animator.SetFloat("Acceleration", strafe);
    }

    public void SetFallVelocity(float velocity)
    {
        _animator.SetFloat("yVelocity", velocity);
    }

    public void SetGrounded(bool state)
    {
        _animator.SetBool("IsGrounded", state);
    }
}
