﻿public interface IPlayerDieObserver
{
    void OnPlayerDie(Player player);
}

interface IPlayerDieObservable
{
    void AddObserver(IPlayerDieObserver observer);
    void RemoveObserver(IPlayerDieObserver observer);
    void OnPlayerDie(Player player);
}