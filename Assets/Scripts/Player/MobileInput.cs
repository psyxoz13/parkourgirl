using UnityEngine;
using UnityEngine.Events;

public class MobileInput : MonoBehaviour
{
    public Vector2 Acceleration { get; private set; }
    public bool IsTouch { get; private set; }

    public UnityEvent OnTap = new UnityEvent();

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnTap?.Invoke();
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Acceleration = Vector2.left * .2f;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            Acceleration = Vector2.right * .2f;
        }
        else
        {
            Acceleration = Vector2.zero;
        }

#else
        Acceleration = Vector2.Lerp(
            Acceleration,
            Input.acceleration,
            5f * Time.deltaTime);

        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                IsTouch = true;
                OnTap?.Invoke();
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                IsTouch = false;
            }
        }
#endif
    }
}
