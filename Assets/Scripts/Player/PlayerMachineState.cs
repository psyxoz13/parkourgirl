using UnityEngine;
using UnityEngine.Events;

public class PlayerMachineState : StateMachineBehaviour
{
    public static UnityEvent<MachineState> OnStateChanged = new UnityEvent<MachineState>();
    public static MachineState CurrentState { get; private set; }

    [SerializeField] private MachineState _state;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        CurrentState = _state;
        OnStateChanged?.Invoke(CurrentState);
    }
}

public enum MachineState
{
    Run,
    Action,
    Scene
}