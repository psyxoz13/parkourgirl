using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class AutoRenderScaleURP : MonoBehaviour
{
    [SerializeField] private bool _isAutoScaling;
    [SerializeField] private float _updatePerMinute = 10f;

    [SerializeField, Range(0f, 2f)] private float _maxScale = 1f;
    [SerializeField, Range(0f, 2f)] private float _minScale = .2f;

    [SerializeField] private float _maxDelta = 0.1f;
    [SerializeField] private float _addScale = 0.1f;

    [Header("Debug")]
    public bool OnGUIRenderScale;
    [SerializeField] private Vector2 _positionOnGUI;

    private UniversalRenderPipelineAsset _universalRenderPipelineAsset;
    private float _currentScale = 1f;
    private float _targetFps;

    private void Awake()
    {
        _universalRenderPipelineAsset = QualitySettings.renderPipeline as UniversalRenderPipelineAsset;

        if (Application.targetFrameRate <= 0f)
        {
            _targetFps = Screen.currentResolution.refreshRate;
        }
        else
        {
            _targetFps = Application.targetFrameRate;
        }

        _currentScale = _universalRenderPipelineAsset.renderScale;
    }

    private void Start()
    {
#if UNITY_EDITOR
        if (_isAutoScaling)
        {
            StartCoroutine(GetRenderScaleUpdate());
        }
#endif
    }

    private IEnumerator GetRenderScaleUpdate()
    {
        yield return new WaitForSecondsRealtime(1f);

        var delay = new WaitForSecondsRealtime(60f / _updatePerMinute);
        while (true)
        {
            yield return delay;

            float fps = 1f / Time.smoothDeltaTime * Time.timeScale;
            float newScale = Mathf.Clamp(fps / _targetFps, _minScale, _maxScale);

            if (newScale < _currentScale)
            {
                float deltaScale = _currentScale - newScale;
                
                if (deltaScale > _maxDelta)
                {
                    deltaScale = _maxDelta;
                }

                newScale = _currentScale - deltaScale;

                _currentScale = Mathf.MoveTowards(
                    _currentScale,
                    newScale,
                    delay.waitTime * Time.deltaTime);

            }
            else
            {
                _currentScale = Mathf.Clamp(
                    _currentScale + _addScale,
                   _minScale,
                   _maxScale);
            }

            _universalRenderPipelineAsset.renderScale = _currentScale;
        }
    }

    private void OnGUI()
    {
        if (OnGUIRenderScale == false)
            return;

        var uiStyle = new GUIStyle
        {
            fontSize = 50
        };
        uiStyle.normal.textColor = Color.red;

        GUI.Label(
            new Rect(
                _positionOnGUI,
                Vector2.one * 50f),
            _currentScale.ToString(),
            uiStyle);
    }
}
