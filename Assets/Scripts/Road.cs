using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Road : MonoBehaviour
{
    [SerializeField] private Chunk _startChunk;
    [SerializeField] private int _chunksCount = 5;
    [SerializeField] private int _uniqueChuncksCount = 4;

    [SerializeField] private Chunk[] _chunksPrefabs;

    private Queue<Chunk> _chunksQueue = new Queue<Chunk>(); 
    private Queue<int> _lastSpawnedIndexes = new Queue<int>();

    private void Start()
    {
        _chunksQueue.Enqueue(_startChunk);

        SpawnStartChunks();
    }

    private void SpawnStartChunks()
    {
        for (int i = 0; i < _chunksCount; i++)
        {
            SpawnChunk();
        }
    }

    private void SpawnChunk()
    {
        var prevChunk = _chunksQueue.Last();

        int index = GetCorrectedIndex();


        var newChunk = Instantiate(
            _chunksPrefabs[index]);

        _chunksQueue.Enqueue(newChunk);

        newChunk.transform.position = prevChunk.CreatePoint.position + Vector3.forward * prevChunk.GroundLenght / 2f;
        newChunk.PlayerTrigger.OnEnter.AddListener(
            (_) =>
            {
                var destroyChunk = _chunksQueue.Dequeue();
                destroyChunk.PlayerTrigger.OnEnter.RemoveAllListeners();
                Destroy(destroyChunk.gameObject);

                SpawnChunk();
            });
    }

    private int GetCorrectedIndex()
    {
        int index = Random.Range(0, _chunksPrefabs.Length);

        if (_lastSpawnedIndexes.Contains(index))
        {
            return GetCorrectedIndex();
        }
        
        if (_lastSpawnedIndexes.Count > _uniqueChuncksCount)
        {
            _lastSpawnedIndexes.Dequeue();
        }
        _lastSpawnedIndexes.Enqueue(index);

        return index;
    }
}
