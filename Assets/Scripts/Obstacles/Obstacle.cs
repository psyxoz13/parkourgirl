using UnityEngine;

public class Obstacle : MonoBehaviour, IAction
{
    public string AnimationTrigger => _animationTrigger;
    public MoveStruct MoveStruct => _moveStruct;

    [SerializeField] private PlayerTrigger _playerTrigger;
    [SerializeField] private string _animationTrigger;
    [SerializeField] private MoveStruct _moveStruct;

    private void Awake()
    {
        _playerTrigger.OnEnter.AddListener(SetPlayerAction);
        _playerTrigger.OnExit.AddListener(SetPlayerDefaultAction);
    }

    public void SetPlayerAction(Player player)
    {
        player.SetCurrentAction(this);
    }

    public void SetPlayerDefaultAction(Player player)
    {
        player.SetDefaultAction();
    }

    private void OnDestroy()
    {
        _playerTrigger.OnEnter.RemoveListener(SetPlayerAction);
        _playerTrigger.OnExit.RemoveListener(SetPlayerDefaultAction);
    }
}
