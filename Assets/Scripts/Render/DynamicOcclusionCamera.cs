using UnityEngine;

[RequireComponent(typeof(Camera))]
public class DynamicOcclusionCamera : MonoBehaviour
{
    public static DynamicOcclusionCamera Instance { get; private set; }

    private Camera _camera;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
        Instance = this;
    }

    private void OnEnable()
    {
        Instance = this;
    }

    public bool IsVisibleIn(Bounds bounds)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(_camera);

        return GeometryUtility.TestPlanesAABB(planes, bounds);
    }
}
