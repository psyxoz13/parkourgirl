using System.Collections.Generic;
using UnityEngine;

public class DynamicOcclusionObject : MonoBehaviour
{
    private List<Renderer> _renderers = new List<Renderer>();

    private Bounds _bounds;
    private Renderer _renderer;
    private bool _enabled;

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
    }

    private void OnEnable()
    {
        SetBounds();
    }

    private void SetBounds()
    {
        var renderers = GetComponentsInChildren<Renderer>();

        _renderers.Clear();
        if (_renderer != null)
        {
            _bounds = _renderer.bounds;
            _renderers.Add(_renderer);
        }
        else if (renderers.Length > 0)
        {
            _bounds = renderers[0].bounds;
        }

        _renderers.AddRange(renderers);
        EncapsulateChildrenBounds(renderers);
    }

    private void EncapsulateChildrenBounds(Renderer[] renderers)
    {
        foreach (Renderer render in renderers)
        {
            _bounds.Encapsulate(render.bounds);
        }
    }

    private void Update()
    {
        bool visible = DynamicOcclusionCamera.Instance.IsVisibleIn(_bounds);

        if (visible != _enabled)
        {
            _enabled = visible;
            SetRenderersState(_enabled);
        }       
    }

    private void SetRenderersState(bool state)
    {
        int count = _renderers.Count;
        for (int i = 0; i < count; i++)
        {
            Renderer renderer = _renderers[i];

            if (renderer == null)
            {
                _renderers.Remove(renderer);
                continue;
            }

            renderer.enabled = state;
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        _renderer = GetComponent<Renderer>();
        SetBounds();

        Gizmos.color = Color.blue;

        Gizmos.DrawWireCube(_bounds.center, _bounds.size);
    }
#endif
}
