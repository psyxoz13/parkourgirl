using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class SceneLoadButton : MonoBehaviour
{
    [SerializeField] private string _sceneName;

    private Button _button;

    private void Awake()
    {
        _button = GetComponent<Button>();
    }

    private void OnEnable()
    {
        _button.onClick.AddListener(Load);
    }

    private void OnDisable()
    {
        _button.onClick.RemoveListener(Load);
    }

    private void Load()
    {
        SceneManager.LoadScene(_sceneName);
    }
}
