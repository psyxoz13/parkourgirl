using DG.Tweening;
using UnityEngine;

public abstract class UIEffect : MonoBehaviour
{
    [SerializeField] protected float _maxValue;
    [SerializeField] protected float _minValue;
    [SerializeField] protected float _duration = 1f;
    [SerializeField] protected Ease _ease;

    [Space]
    [SerializeField] private bool _playOnAwake;

    private void Awake()
    {
        if (_playOnAwake)
        {
            GetPrestartTween()
                .OnComplete(() => Play());
        }
    }

    public abstract void Play();
    protected abstract Tween GetPrestartTween();
}
