using UnityEngine;
using UnityEngine.Events;

public class UIMenu : MonoBehaviour
{
    public UIMenuState MenuState;

    public UnityEvent OnActivate = new UnityEvent();
    public UnityEvent OnDeactivate = new UnityEvent();

    public virtual void Activate()
    {
        OnActivate?.Invoke();
    }

    public virtual void Deactivate()
    {
        OnDeactivate?.Invoke();
    }
}
