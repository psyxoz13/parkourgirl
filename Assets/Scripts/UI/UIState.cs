using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class UIState : MonoBehaviour, IPlayerDieObserver
{
    [SerializeField] private UIMenu[] _uiMenus;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();  
    }

    private void OnEnable()
    {
        Player.PlayerDieObservable.AddObserver(this);
    }

    private void OnDisable()
    {
        Player.PlayerDieObservable.RemoveObserver(this);
    }

    public void OnPlayerDie(Player player)
    {
        ActivateConcreteState(UIMenuState.Dead);

        _animator.SetTrigger("SetDeadMenu");
    }

    private void ActivateConcreteState(UIMenuState menuState)
    {
        for (int i = 0; i < _uiMenus.Length; i++)
        {
            var menu = _uiMenus[i];

            if (menu.MenuState == menuState)
            {
                menu.gameObject.SetActive(true);
                menu.Activate();
                continue;
            }

            menu.Deactivate();
            menu.gameObject.SetActive(false);
        }
    }
}
