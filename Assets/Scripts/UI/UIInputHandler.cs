using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class UIInputHandler : MonoBehaviour, IPointerClickHandler
{
    public UnityEvent OnClick = new UnityEvent();

    public void OnPointerClick(PointerEventData eventData)
    {
        OnClick?.Invoke();
    }
}
