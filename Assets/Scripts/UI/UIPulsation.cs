using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class UIPulsation : UIEffect
{
    public override void Play()
    {
        var sequence = DOTween.Sequence();

        sequence.SetEase(_ease);
        sequence.Append(transform.DOScale(_maxValue, _duration));
        sequence.Append(transform.DOScale(_minValue, _duration));
        sequence.SetLoops(-1);
    }

    protected override Tween GetPrestartTween()
    {
        return transform.DOScale(_minValue, _duration / 2);
    }
}
