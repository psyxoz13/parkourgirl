using DG.Tweening;
using UnityEngine;

public class UISway : UIEffect
{
    public override void Play()
    {
        var sequence = DOTween.Sequence();

        sequence.SetEase(_ease);
        sequence.Append(transform.DOLocalRotate(Vector3.forward * _maxValue, _duration));
        sequence.Append(transform.DOLocalRotate(Vector3.forward * _minValue, _duration));
        sequence.SetLoops(-1);
    }

    protected override Tween GetPrestartTween()
    {
        return transform.DOLocalRotate(Vector3.forward * _minValue, _duration / 2);
    }
}
