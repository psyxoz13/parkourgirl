public interface IAction
{
    public string AnimationTrigger { get; }
    public MoveStruct MoveStruct { get; }

    public void SetPlayerAction(Player player);
    public void SetPlayerDefaultAction(Player player);
}
