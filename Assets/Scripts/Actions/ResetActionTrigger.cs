using UnityEngine;

[RequireComponent(typeof(PlayerTrigger))]
public class ResetActionTrigger : MonoBehaviour, IAction
{
    public string AnimationTrigger => _defaultAction.AnimationTrigger;
    public MoveStruct MoveStruct => _defaultAction.MoveStruct;

    private PlayerTrigger _playerTrigger;
    private IAction _defaultAction = new DefaultAction();

    private void Awake()
    {
        _playerTrigger = GetComponent<PlayerTrigger>();
    }

    private void OnEnable()
    {
        _playerTrigger.OnEnter.AddListener(SetPlayerDefaultAction);
    }

    private void OnDisable()
    {
        _playerTrigger.OnEnter.RemoveListener(SetPlayerDefaultAction);
    }

    public void SetPlayerDefaultAction(Player player)
    {
        player.SetDefaultAction();
    }

    public void SetPlayerAction(Player player)
    {

    }
}
