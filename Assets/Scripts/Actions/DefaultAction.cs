public class DefaultAction : IAction
{
    public string AnimationTrigger => "SetJump";

    public MoveStruct MoveStruct => new MoveStruct()
    { 
        ForceMultiplier = 1,
        Height = .6f
    };

    public void SetPlayerAction(Player player)
    {
        player.SetCurrentAction(this);
    }

    public void SetPlayerDefaultAction(Player player)
    {
        player.SetDefaultAction();
    }
}
