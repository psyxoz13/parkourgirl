using UnityEngine;

[RequireComponent(typeof(PlayerTrigger))]
public class PlayerActionTrigger : MonoBehaviour, IAction
{
    public string AnimationTrigger => _animationTrigger;
    public MoveStruct MoveStruct => _moveStruct;

    [SerializeField] private string _animationTrigger;
    [SerializeField] private MoveStruct _moveStruct;

    private PlayerTrigger _playerTrigger;

    private void Awake()
    {
        _playerTrigger = GetComponent<PlayerTrigger>();
    }

    private void OnEnable()
    {
        _playerTrigger.OnEnter.AddListener(SetPlayerAction);
        _playerTrigger.OnExit.AddListener(SetPlayerDefaultAction);
    }

    private void OnDisable()
    {
        _playerTrigger.OnEnter.RemoveListener(SetPlayerAction);
        _playerTrigger.OnExit.RemoveListener(SetPlayerDefaultAction);
    }

    public void SetPlayerAction(Player player)
    {
        player.SetCurrentAction(this);
    }

    public void SetPlayerDefaultAction(Player player)
    {
        player.SetDefaultAction();
    }
}
